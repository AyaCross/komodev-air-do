class PgalonController < ApplicationController
	def gkonfirm
		@galon = Galon.find(params[:id])
	end

	def gconfirm
		pgalon = Pgalon.new(pgalon_params)
		if pgalon.save
			redirect_to '/gstatus'
		else
			redirect_to '/gkonfirm'
		end
	end

	def status
		@pgalon = current_pgalon
	end

	private
	def pgalon_params
		params.require(:pgalon).permit(:gid, :harga)
	end
end
