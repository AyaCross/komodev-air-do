class RegisterController < ApplicationController
	def new
	end

	def create
    user = User.new(user_params)
    if user.save
      session[:user_id] = user.id
      redirect_to '/'
    else
      redirect_to '/register'
    end
  end

  def profile
    @user = current_user
  end

  def edit
    @user = current_user
  end
  
  def update
    @user = current_user
    if @user.update_attributes(user_params)
      redirect_to '/profile'
    else
      render 'edit'
    end
  end

  def epass
    @user = current_user
  end

  def uppass
    @user = current_user
    if @user.update_attributes(pass_params)
      redirect_to '/'
    else
      render 'epass'
    end
  end

  private
  def user_params
    params.require(:user).permit(:fnama, :nama, :telp, :alamat, :password, :password_confirmation)
  end

  def profile_params
    params.require(:user).permit(:fnama, :nama, :telp, :alamat)
  end

  def pass_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end