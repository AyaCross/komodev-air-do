class ApplicationController < ActionController::Base
	protect_from_forgery prepend:true, only: [:create]

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end
	helper_method :current_user

	def authorize
		redirect_to '/login' unless current_user
	end

	def current_admin
		@current_admin ||= Admin.find(session[:admin_id]) if session[:admin_id]
	end
	helper_method :current_admin
end
