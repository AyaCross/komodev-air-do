class AddStatusToGalons < ActiveRecord::Migration[5.2]
  def change
    add_column :galons, :Status, :string
    add_column :galons, :User_ID, :integer
  end
end
