class AddStatusToTangkis < ActiveRecord::Migration[5.2]
  def change
    add_column :tangkis, :Status, :string
    add_column :tangkis, :User_ID, :integer
  end
end
