class CreateTangkis < ActiveRecord::Migration[5.2]
  def change
    create_table :tangkis do |t|
      t.string :liter
      t.string :telp
      t.text :alamat

      t.timestamps
    end
  end
end
