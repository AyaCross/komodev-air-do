Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount ForestLiana::Engine => '/forest'
	get 'home' => 'home#home', as: 'home'

	get 'alogin' => 'session#login'
	post 'alogin' => 'session#create'
	get 'alogout' => 'session#destroy'

	get 'register' => 'register#new'
	post 'signup' => 'register#create'
	get 'profile' => 'register#profile'
	get 'edit' => 'register#edit'
	post 'edit' => 'register#update'
	get 'epass' => 'register#epass'
	post 'epass' => 'register#uppass'

	get 'login' => 'login#new'
	post 'login' => 'login#create'
	get 'logout' => 'login#destroy'

	get 'gindex' => 'galon#index'
	get 'gform' => 'galon#new'
	post 'gsave' => 'galon#create'

	get 'tindex' => 'tangki#index'
	get 'tform' => 'tangki#new'
	post 'tsave' => 'tangki#create'

	resources :galon, :only => [:show]

	resources :tangki, :only => [:show]

	root 'home#home'
end
